<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    public function random($count)
    {
    	$blogs = Blog::select('*')
    		->inRandomOrder()
    		->limit($count)
    		->get();
    	$data['blogs'] = $blogs;
    	
    	return response()->json([
    		'response_code' => '00',
    		'response_message' => 'data campaign berhasil di tampilkan',
    		'data' 	=> $data
    	], 200);
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'title' => 'required',
    		'description' => 'required',
    		'image' => 'required|mimes:jpg,jpeg,png'
    	]);

    	$blog = Blog::create([
    		'title' => $request->title,
    		'description' => $request->description,
    		'image' => $request->image,
    	]);

    	
    	$data['blog'] = $blog;

    	return response()->json([
    		'response_code' => '00',
    		'response_message' => 'data campaigns berhasil ditambahkan',
    		'data' => $data
    	], 200);
    }
}
