<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile\Profile;
use App\Http\Resources\ProfileResoure;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'              => ['required', 'min:3', 'max:255'],
            'address'           => ['required'],
            'profile_picture'   => ['required'],
            'subject'           => ['required'],

        ]);

        $profiles = auth()->user()->profiles()->create($this->profileStore());

        return $profiles;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return new ProfileResoure($profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {

        $profile->update($this->profileStore());

        return new ProfileResoure($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profileStore()

    {
        return[
            
            'name'                  => request('name'),
            'slug'                  => \Str::slug(request('name')),
            'address'               => request('address'),
            'profile_picture'       => request('profile_picture'),
            'subject_id'            => request('subject'),
        ];
    }
}
