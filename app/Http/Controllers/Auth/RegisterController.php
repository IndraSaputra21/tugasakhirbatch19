<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\UserRegisterdMail;
use Mail;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //$data =[];

        $user = User::create([
            'name'     => request('name'),
            'username' => request('username'),
            'email'    => request('email'),
            'password' => bcrypt(request('password')),
        ]);

        //$data['user'] = $user;

        Mail::to($user)->send(new UserRegisterdMail($user));

        return response('Thanks, you are registered.');
    }
}
