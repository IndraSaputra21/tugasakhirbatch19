<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaigns = [];
        $faker = Faker\Factory::create();
        for($i=0; $i<5; $i++) {
        	$avatar_path = 'public/images/campaigns';
        	$avatar_fullpath = $faker->image($avatar_path,200, 250, 'people', true, true, 'people');
        	$avatar_image = explode("\\",$avatar_fullpath);
        	$campaigns[$i] = [
        		'title' => $faker->title,
        		'description' =>$faker->text,
        		'image' => 'images/campaigns/' . dd($avatar_image),//$avatar_image[1],
        		'created_at' =>Carbon\Carbon::now(),
        	];
        }
        DB::table('campaigns')->insert($campaigns);
    }
}
